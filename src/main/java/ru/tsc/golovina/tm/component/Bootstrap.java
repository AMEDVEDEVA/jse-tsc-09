package ru.tsc.golovina.tm.component;

import ru.tsc.golovina.tm.api.ICommandController;
import ru.tsc.golovina.tm.api.ICommandRepository;
import ru.tsc.golovina.tm.api.ICommandService;
import ru.tsc.golovina.tm.constant.ArgumentConst;
import ru.tsc.golovina.tm.constant.TerminalConst;
import ru.tsc.golovina.tm.controller.CommandController;
import ru.tsc.golovina.tm.repository.CommandRepository;
import ru.tsc.golovina.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void start(String[] args) {
        displayWelcome();
        parseArgs(args);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    private void displayWelcome() {
        System.out.println("---Welcome to task manager---");
    }

    public void parseArgs(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        commandController.exit(0);
    }

    public void parseArg(final String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            default:
                commandController.showArgumentError();
                commandController.exit(1);
        }
    }

    public void parseCommand(final String command) {
        switch (command) {
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.EXIT:
                commandController.exit(0);
                break;
            default:
                commandController.showCommandError();
        }
    }

}
