package ru.tsc.golovina.tm.controller;

import ru.tsc.golovina.tm.api.ICommandController;
import ru.tsc.golovina.tm.api.ICommandService;
import ru.tsc.golovina.tm.model.Command;
import ru.tsc.golovina.tm.util.NumberUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getCommands();
        for (final Command command : commands)
            System.out.println(command);
    }

    @Override
    public void showCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = commandService.getCommands();
        for (final Command command : commands)
            showCommandValue(command.getName());
    }

    @Override
    public void showArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = commandService.getCommands();
        for (final Command command : commands)
            showCommandValue(command.getArgument());

    }

    private void showCommandValue(final String value) {
        if (value == null || value.isEmpty())
            return;
        System.out.println(value);
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.9.0");
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Developer: Alla Golovina");
        System.out.println("e-mail: amedvedeva@tsconsulting.com");
    }

    @Override
    public void showInfo() {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory: " + NumberUtil.formatBytes(usedMemory));
    }

    @Override
    public void showArgumentError() {
        System.err.println("Error! Argument not found...");
    }

    @Override
    public void showCommandError() {
        System.err.println("Error! Command not found...");
    }

    @Override
    public void exit(int status) {
        System.exit(status);
    }

}
