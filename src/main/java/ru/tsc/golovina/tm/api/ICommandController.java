package ru.tsc.golovina.tm.api;

public interface ICommandController {

    void showHelp();

    void showCommands();

    void showArguments();

    void showVersion();

    void showAbout();

    void showInfo();

    void showArgumentError();

    void showCommandError();

    void exit(int status);
}
